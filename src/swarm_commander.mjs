import * as THREE from '../build/three.module.js';
import {BaseCommander} from "./base_commander.mjs"

function _base64ToArrayBuffer(base64) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
      bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes;
}

function tnow() {
    return new Date().getTime() / 1000;
}
  
let vaild_ids = new Set([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
class SwarmCommander extends BaseCommander{
    constructor(ui) {
        super(ui);
        this.mav = new MAVLink(null, 0, 0);
        this.origin_drone_id = [0,1,2,3];
        this.select_id = -1;

        this._lps_time = 0;

        this.fly_time = {}
        this.last_sum_fly_time = {}
        this.start_time = {}
        this.online_time = {}
        this.armed_time = {}

        this.ui.cmder = this;        

        this.landing_speed = 0.2;

        this.last_recv_pcl = tnow();
        this.pcl_duration = 0.3;

        this.current_formation = 0;
        this.status = {}
        this.pose_valid = {}
        this.pose_subs={}
        this.state_subs={}
        this.missions = {}

        this.base_pose = [[0,4.5,0],[0,1.5,0],[0,-1.5,0],[0,-4.5,0]];
        this.average_pose = {}
        this.uav_pos = {}
        this.uav_quat = {}

        this.mission_update();
    }
    
    sub_vicon_id(i) {
        console.log("subscribing vicon "+ i);
        var vicon_sub = new ROSLIB.Topic({
            ros: this.ros,
            name: "/swarm_mocap/SwarmNodePose" + i,
            messageType: "geometry_msgs/PoseStamped"
        });
        
        let _id = i;
        let self = this;
        this.vicon_subs[_id] = (vicon_sub);
        vicon_sub.subscribe(function (incoming_msg) {
            self.on_vicon_msg(_id, incoming_msg);
        });
    }

    // i = null , -1 测试
    sub_local_pos_id(i=-1) {
        console.log("subscribing local_pos "+ i);
        var local_pose_sub = new ROSLIB.Topic({
            ros: this.ros,
            name: i<0?"/mavros/local_position/pose":"/uav"+i+"/mavros/local_position/pose",
            messageType: "geometry_msgs/PoseStamped"
        });

        let _id = i;
        let self = this;
        this.pose_subs[_id] = (local_pose_sub);
        local_pose_sub.subscribe(function (incoming_msg) {
            self.on_local_pose_msg(_id, incoming_msg);
        });
    }

    sub_mavros_state_id(i=-1) {
        console.log("subscribing mavros_state "+ i);
        var mavros_state_sub = new ROSLIB.Topic({
            ros: this.ros,
            name: i<0?"/mavros/state":"/uav"+i+"/mavros/state",
            messageType: "mavros_msgs/State",
            queue_length:1
        });

        let _id = i;
        let self = this;
        this.state_subs[_id] = (mavros_state_sub);
        mavros_state_sub.subscribe(function (state_msg) {
            self.on_state_msg(_id,state_msg);
        });
    }

    setup_ros_sub_pub() {
        let ros = this.ros;
        let self = this;

        this.ui.set_display_mode("GLOBAL");
        this.ui.recal_pos();

        this.sub_mavros_state_id(0);
        this.sub_mavros_state_id(1);
        this.sub_mavros_state_id(2);
        this.sub_mavros_state_id(3);

        this.sub_local_pos_id(0);
        this.sub_local_pos_id(1);
        this.sub_local_pos_id(2);
        this.sub_local_pos_id(3);

        // this.remote_nodes_listener = new ROSLIB.Topic({
        //     ros: ros,
        //     name: "/uwb_node/remote_nodes",
        //     messageType: "inf_uwb_ros/remote_uwb_info",
        //     queue_length:1
        //   });
          
        // this.remote_nodes_listener.subscribe(function(msg) {
        //     self.on_remote_nodes_info(msg);
        // });

        // this.traj_viz_listener = new ROSLIB.Topic({
        //     ros: ros,
        //     name: "/traj_viz",
        //     messageType: "visualization_msgs/Marker",
        //     queue_length:10
        // });
        
        // this.traj_viz_listener.subscribe(function (msg) {
        //     // console.log(msg);
        //     self.ui.update_drone_traj(msg.ns, msg.points)
        // });
        //
        // this.bspine_viz_listener = new ROSLIB.Topic({
        //     ros: ros,
        //     name: "/planning/bspline",
        //     messageType: "bspline/Bspline",
        //     queue_length:10
        // });
        
        // this.bspine_viz_listener.subscribe(function (msg) {
        //     self.ui.update_drone_traj_bspline("debug", msg)
        // });

        // this.incoming_data_listener = new ROSLIB.Topic({
        //     ros: ros,
        //     name: "/uwb_node/incoming_broadcast_data",
        //     messageType: "inf_uwb_ros/incoming_broadcast_data",
        //     queue_length:1
        // });
        //
        //
        //
        // this.incoming_data_listener.subscribe(function (incoming_msg) {
        //     self.on_incoming_data(incoming_msg);
        // });
        //



        // this.vicon_subs = {
        //     // 2: this.sub_vicon_id(2),
        //     // 0: this.sub_vicon_id(0)
        // }
       
        // this.send_uwb_msg = new ROSLIB.Topic({
        //     ros : ros,
        //     name : '/uwb_node/send_broadcast_data',
        //     messageType : 'inf_uwb_ros/data_buffer'
        // });

       
        // this.send_uwb_msg = new ROSLIB.Topic({
        //     ros : ros,
        //     name : '/uwb_node/send_broadcast_data',
        //     messageType : 'inf_uwb_ros/data_buffer'
        // });
        //
        // this.change_formation_client = new ROSLIB.Service({
        //     ros : ros,
        //     name : '/transformation',
        //     serviceType : 'swarm_transformation/transformation'
        // });
        //
        // this.translation_flyto_client = new ROSLIB.Service({
        //     ros : ros,
        //     name : '/translation',
        //     serviceType : 'swarm_transformation/translation'
        // });

    }

    on_state_msg(i,state_msg){
        // console.log(state_msg,i);
        let cur_time = state_msg.header.stamp.secs+state_msg.header.stamp.nsecs/1e9;

        if(this.start_time[i]==null){
            this.start_time[i] = cur_time;
         }

        if(!state_msg.connected){
            this.fly_time[i] = 0.0;
            this.online_time[i] = 0.0;
            this.armed_time[i] = 0.0;
        }

        if(!state_msg.armed){
            this.armed_time[i] = cur_time;
            this.last_sum_fly_time = this.fly_time[i]?this.fly_time[i]:0;
        }else{
            if(this.armed_time[i]==null){
                this.armed_time[i] = cur_time;
            }
            if(this.fly_time[i]!=null){
                this.fly_time[i]=this.last_sum_fly_time[i]?
                    this.last_sum_fly_time[i]+(cur_time-this.armed_time[i]):
                    (cur_time-this.armed_time[i]);
            }else{
                this.fly_time[i] = 0.0;
            }
        }

        // console.log(this.fly_time[i]);
        // console.log(this.fly_time[i]);
        // status
        let status = {
            online_time: cur_time-this.start_time[i],
            fly_time: this.fly_time[i],
            manual_input: state_msg.manual_input,
            guide_status: state_msg.guided,
            connected_status: state_msg.connected,
            ctrl_from: (state_msg.mode == "OFFBOARD"),
            ctrl_mode: state_msg.mode,
            arm_status: state_msg.armed,
        }
        this.status[i] = {}
        this.status[i] = status
        // console.log(this.status);
    }

    get_status(_id){
        return _id<0? this.status:this.status[_id];
    }

 
    on_vicon_msg(_id, msg) {
        // msg.qua
        var euler = new THREE.Euler(0, 2.34, 0);

        let _q = msg.pose.orientation;
        var quat = new THREE.Quaternion(_q.x, _q.y, _q.z, _q.w);
        var pos = new THREE.Vector3(msg.pose.position.x, msg.pose.position.y, msg.pose.position.z);

        this.ui.update_drone_globalpose(_id, pos, quat);
    }

    on_local_pose_msg(_id, msg) {
        // console.log(msg);
        let _q = msg.pose.orientation;
        var quat = new THREE.Quaternion(_q.x, _q.y, _q.z, _q.w);
        var pos = new THREE.Vector3(msg.pose.position.x, msg.pose.position.y, msg.pose.position.z);
        this.uav_pos[_id] = pos;
        this.uav_quat[_id] = quat;
        let global_pos = pos;
        global_pos.x += this.base_pose[_id][0];
        global_pos.y += this.base_pose[_id][1];
        global_pos.z += this.base_pose[_id][2];
        this.ui.update_drone_globalpose(_id, pos, quat);
        this.status[_id].x = pos.x;
        this.status[_id].y = pos.y;
        this.status[_id].z = pos.z;
        this.pose_valid[_id] = !(pos.x<0.01&&pos.y<0.01);
        this.ui.set_drone_status(_id, this.status[_id]);
    }



    on_incoming_data(incoming_msg) {
        // console.log(incoming_msg);
        if (!vaild_ids.has(incoming_msg.remote_id)) {
            return;
        }
        
        let ts = tnow();
        //note that message may come from different nodes, should fix here
        let buf = _base64ToArrayBuffer(incoming_msg.data);
        // console.log(buf);
        let msgs = this.mav.parseBuffer(buf);
        // console.log(msgs);
        for (var k in msgs) {
          let msg = msgs[k];
            switch (msg.name) {
                case "NODE_REALTIME_INFO": {
                    // this.on_drone_realtime_info_recv(incoming_msg.remote_id, incoming_msg.lps_time, msg);
                    break;
                }

                case "DRONE_STATUS": {
                    // console.log(msg);
                    this.on_drone_status_recv(incoming_msg.remote_id, incoming_msg.lps_time, msg);
                    break;
                }
                case "NODE_LOCAL_FUSED" : {
                    // console.log(msg);
                    this.on_node_local_fused(incoming_msg.remote_id, incoming_msg.lps_time, msg);
                    break;
                }

                case "NODE_BASED_FUSED": {
                    // console.log(msg);
                    this.on_node_based_coorindate(incoming_msg.remote_id, incoming_msg.lps_time, msg);
                    break;
                }
                // console.log(msg);

            }
        }
        let dt = tnow() - ts;
        // console.log("Process time ", dt*1000);
    }

    on_node_local_fused(_id, lps_time, msg) {
        // console.log(msg);    
        var pos = new THREE.Vector3(msg.x/1000.0, msg.y/1000.0, msg.z/1000.0);
        var quat = new THREE.Quaternion();
        quat.setFromEuler(new THREE.Euler(0, 0, msg.yaw/1000.0));

        this.ui.update_drone_localpose_in_coorinate(msg.target_id, pos, quat, _id, msg.cov_x/1000.0, msg.cov_y/1000.0, msg.cov_z/1000.0, msg.cov_yaw/1000.0);
    }

    on_node_based_coorindate(_id, lps_time, msg) {
        // console.log(msg);
        // console.log("Based ", _id,"->" ,msg.target_id);
        this.ui.update_drone_based_coordinate(msg.target_id, msg.rel_x/1000.0, msg.rel_y/1000.0,
            msg.rel_z/1000.0, msg.rel_yaw_offset/1000.0, _id, msg.cov_x/1000.0, msg.cov_y/1000.0, msg.cov_z/1000.0, msg.cov_yaw/1000.0);
    }


    on_remote_nodes_info(msg) {
        var avail = 0;
        for (var i in msg.active) {
          // console.log(i);
          avail += msg.active[i];
        }
        this.ui.set_available_drone_num(avail);
        this.ui.set_total_drone_num(msg.node_ids.length);
        this.ui.set_lps_time(msg.sys_time);
        this.ui.set_self_id(msg.self_id);
    }

    on_drone_status_recv(_id, lps_time, status) {

        // console.log(status);
        if (! (_id in this.vicon_subs) && this.ui.global_local_mode ) {
            this.sub_vicon_id(_id);
        }

        this.ui.set_drone_status(_id, status)

        var pos = new THREE.Vector3(status.x, status.y, status.z);
        var quat = new THREE.Quaternion();
        quat.setFromEuler(new THREE.Euler(0, 0, status.yaw));
        // this.ui.update_drone_selfpose(_id, pos, quat, 0, 0, 0);
        this.uav_pos[_id] = pos;

        // console.log("Update --", _id, pos, quat);

        // this.ui.set_bat_level(_id, status.bat_vol);
        // this.ui.set_drone_lps_time(_id, lps_time);
        // this.ui.set_drone_control_auth(_id, status.ctrl_auth);
        // this.ui.set_drone_control_mode(_id, status.ctrl_mode);
        // this.ui.set_drone_selfpose(status.x, status.y, status.z, 0, 0, 0);
    }

    // on_drone_realtime_info_recv(_id, lps_time, info) {
    //     // console.log(info);
    //     var pos = new THREE.Vector3(info.x, info.y, info.z);
    //     var quat = new THREE.Quaternion();
    //     quat.set(info.qx,info.qy,info.qz,info.qw);
    //     this.ui.update_drone_selfpose(_id, pos, quat);
    //     this.uav_pos[_id] = pos;
    // }

    send_takeoff_cmd(_id) {
        this.stop_transformation_thread();
        console.log("Will send takeoff command");
        let takeoff_cmd = 5;
        let scmd = new mavlink.messages.swarm_remote_command (this.lps_time, _id,  takeoff_cmd, 10000, 15000, 0, 0, 0, 0, 0, 0, 0, 0);
        this.send_msg_to_swarm(scmd);
    }

    send_landing_cmd(_id) {
        this.stop_transformation_thread();
        console.log("Will send landing command");
        let landing_cmd = 6;
        let scmd = new mavlink.messages.swarm_remote_command (this.lps_time, _id, landing_cmd, 0, this.landing_speed *10000, 0, 0, 0, 0, 0, 0, 0, 0);
        this.send_msg_to_swarm(scmd);
    }

    send_flyto_cmd(_id, pos, direct) {
        //When use VO coordinates
        // console.log("Fly to ", pos);
        var flyto_cmd = 0;
        if (! direct) {
            flyto_cmd = 10;
        }
        let scmd = new mavlink.messages.swarm_remote_command (this.lps_time, _id, flyto_cmd, 
            Math.floor(pos.x*10000), 
            Math.floor(pos.y*10000), 
            Math.floor(pos.z*10000), 0, 0, 0, 0, 0, 0, 0);
        this.send_msg_to_swarm(scmd);
    }

    send_emergency_cmd() {
        this.stop_transformation_thread();
        console.log("Will send emergency command");
        let landing_cmd = 6;
        let scmd = new mavlink.messages.swarm_remote_command (this.lps_time, -1, landing_cmd, -1, 10000, 0, 0, 0, 0, 0, 0, 0, 0);
        this.send_msg_to_swarm(scmd);
    }

    send_traj_cmd(_id, cmd) {
        console.log("send traj", cmd);
        let scmd = new mavlink.messages.swarm_remote_command (this.lps_time, _id, 100+cmd, 
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        this.send_msg_to_swarm(scmd);
    }

    send_msg_to_swarm(_msg) {
        let _data = _msg.pack(this.mav);
        var msg = new ROSLIB.Message({data : _data, send_method: 2});
        this.send_uwb_msg.publish(msg);
    }

    // start_circle_fly(_id, origin, r=1, T=10, yaw_mode="fixed") {
    //     if (_id < 0) {
    //         return;
    //     }
    //     if (origin == null) {
    //         origin = {
    //             x:this.uav_pos[_id].x,
    //             y:this.uav_pos[_id].y + r,
    //             z:this.uav_pos[_id].z
    //         }
    //     }
    //
    //     this.missions[_id] = {
    //         "mission": "circle",
    //         "origin": origin,
    //         "T": T,
    //         "ts": tnow(),
    //         "r": r,
    //         "yaw_mode": yaw_mode
    //     }
    // }
    //
    // stop_mission_id(_id) {
    //     if (_id == -1) {
    //         this.missions = {};
    //     }
    //     delete this.missions[_id];
    // }

    // circle_mission(_id, mission, _tnow) {
    //     // console.log("circle mission");
    //     let flyto_cmd = 0;
    //     let t = _tnow - mission.ts;
    //     let r = mission.r;
    //     let yaw_mode = mission.yaw_mode;
    //     let ox = mission.origin.x;
    //     let oy = mission.origin.y;
    //     let oz = mission.origin.z;
    //     let T = mission.T;
    //
    //     let pi = Math.PI;
    //     let x = ox + Math.sin(t*pi*2/T)*r;
    //     let y = oy - Math.cos(t*pi*2/T)*r;
    //     let vx = Math.cos(t*pi*2/T) * r * pi*2/T;
    //     let vy = Math.sin(t*pi*2/T) * r * pi*2/T;
    //     let ax = - Math.sin(t*pi*2/T) * r * pi*2/T * pi*2/T;
    //     let ay = Math.cos(t*pi*2/T) * r * pi*2/T * pi*2/T;
    //     // console.log(x, y, oz);
    //
    //     let param1 = Math.floor(x*10000)
    //     let param2 = Math.floor(y*10000)
    //     let param3 = Math.floor(oz*10000)
    //     let param5 = Math.floor(vx*10000)
    //     let param6 = Math.floor(vy*10000)
    //     let param7 = 0
    //     let param4 = 666666;
    //     if (yaw_mode == "follow") {
    //         param4 = Math.floor(-t*pi*2*10000/T);
    //     }
    //     var param8 = Math.floor(ax*10000)
    //     var param9 = Math.floor(ay*10000)
    //
    //     let scmd = new mavlink.messages.swarm_remote_command (this.lps_time, _id, flyto_cmd,
    //         param1, param2, param3, param4,
    //         param5, param6, param7, param8, param9, 0);
    //     this.send_msg_to_swarm(scmd);
    // }

    mission_update() {
        let self = this;
        setTimeout(function() {
            // 每隔一段时间 执行函数
            self.mission_update();
        }, 30);
    }

    formation_flyto(pos) {
        if (this.current_formation < 0) {
            return;
        }
        
        var request = new ROSLIB.ServiceRequest({
            next_formation: this.current_formation,
            des_x: pos.x,
            des_y: pos.y,
            des_z: pos.z
        });

        console.log("Target pos", pos.x, pos.y, pos.z);
        let obj = this;
        this.translation_flyto_client.callService(request, function(result) {
            console.log(result);
            obj.current_formation = result.current_formation;
        });
    }

    request_transformation_change(next_trans) {
        console.log("Try to request formation, ", next_trans);
        if (this.current_formation < 0) {
            next_trans = next_trans + 100;
        }

        var request = new ROSLIB.ServiceRequest({
            next_formation: next_trans
        });
        
        let obj = this;
        // 根据当前信息发出请求
        this.change_formation_client.callService(request, function(result) {
            console.log(result);
            obj.ui.set_active_formation(result.current_formation, 0);

            setTimeout(function() {
                obj.ui.set_active_formation(result.current_formation, 1);
                obj.current_formation = result.current_formation;

                obj.ui.clear_drone_trajs();
            }, result.period*1000);
        });
    }

    stop_transformation_thread() {
        console.log("Try to stop formation thread");
        var request = new ROSLIB.ServiceRequest({
            next_formation: -1
        });
        let obj = this;        
        this.change_formation_client.callService(request, function(result) {
            console.log(result);
            obj.current_formation = result.current_formation;
        });
    }
}




// module.exports = {
//     SwarmCommander:SwarmCommander,
//     SwarmGCSUI:SwarmGCSUI
// }
export {SwarmCommander}
