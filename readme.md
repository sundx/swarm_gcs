# 导弹模拟 仿真、地面站、实际

Created: Jun 22, 2020 3:34 PM
Status: 在做的
Tags: 地面站, 文件 教程 记录

无人机检查

1. 注意观察无人机所有连接线是否正常，信号灯是否正常
2. 观察遥控开机后，每个接收机的信号灯是否为常绿，代表遥控链接正常，遥控器电压要大于4.2v
3. 观察无人机GPS灯是否正常，GPS P900数传是否正常连接，进入地面站查看无人机GPS信息要注意看是否已经搜到星，只能在console里面看，其他回传GPS的信息被我关了。或者查看local_position ros消息看是否有x，y数据，代表有定位
4. 注意新的无人机参数要改的内容 ，supply_chk关掉，mav设置 telem2 对应波特率设置57600 hgt 高度设置 测量来源 range sensor 测试mavros是否能正常连接
5. mavros 连接的外部hub需要按照顺序，逐个点亮0123连接，可在电脑上依次验证，ls /dev/tty*
6. 项目地址 [https://gitee.com/sundx/swarm_ws](https://gitee.com/sundx/swarm_ws)
7. 别忘了 source 
8. 运行 launch文件 [https://gitee.com/sundx/swarm_ws/blob/master/src/gps_vicon_cont/launch/swarm/swarm_0123_mavros.launch](https://gitee.com/sundx/swarm_ws/blob/master/src/gps_vicon_cont/launch/swarm/swarm_0123_mavros.launch) 对应会出现四个heartbeat的消息，或者依次检查  /uav0/mavros/state，/uav1/mavros/state等等，里面有个消息为connect 为true 代表该架飞机连接正常
9. 可以运行 轨迹文件 [https://gitee.com/sundx/swarm_ws/blob/master/src/px4_gcs/launch/swarm/swarm_4to1.launch](https://gitee.com/sundx/swarm_ws/blob/master/src/px4_gcs/launch/swarm/swarm_4to1.launch)
10. 内部对应有，是否自动启动，是否完成后自动着落 等，x，y，z 各方向对应的放缩尺度，轨迹文件地址，等。
11. 飞行前，检查工作务必做好

# 启动仿真程序步骤

进入 px4//firmware 目录下

source px4 包:

```bash
source Tools/setup_gazebo.bash $(pwd) $(pwd)/build/px4_sitl_default
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:$(pwd):$(pwd)/Tools/sitl_gazebo
```

之后运行launch文件

```bash
roslaunch px4 multi_uav_mavros_sitl.launch
```

此时gazebo运行 加载出四架无人机

运行地面站服务器 并打开网页：

```bash
roslaunch rosbridge_server rosbridge_websocket.launch
```

顺利的话出现如下：

随后运行swarm程序

```bash
roslaunch px4_gcs sitl_traj_swarm.launch
```

此时 无人机 根据配置文件 进行是否解锁 是否飞行轨迹 是否 完成后降落

# 地面站配置

初次使用需要联网下载部分文件 后续不需要

bson 问题

```bash
sudo apt-get purge python-bson
```

```bash
sudo apt-get intsall python-bson
```

roslibjs + rosbridge_suite

安装：

```bash
sudo apt-get intsall ros-melodic-rosbridge-suite
```

运行：

```bash
roslaunch rosbridge_server rosbridge_websocket.launch
```

打开网页即可 上方 ros连接正常 说明程序正常

地面站样子

仿真gazebo 内容：
